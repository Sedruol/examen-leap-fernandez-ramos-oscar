using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExecuteEvent : MonoBehaviour
{
    [SerializeField] private Text txtEvent;
    [SerializeField] private Image imgBackground;
    [SerializeField] private Image imgCharacter;
    [SerializeField] private AudioSource soundController;
    [SerializeField] private AudioSource soundController2;
    [SerializeField] private AudioClip ost;
    [SerializeField] private AudioClip sfx;
    [SerializeField] private Button btnEvent;
    public const string nameFile = "novel";
    private Data data;
    private int numberEvent;
    // Start is called before the first frame update
    void Start()
    {
        numberEvent = 0;
        btnEvent.onClick.AddListener(() => NewEvent());
        var dataFound = LoadData.Load<Data>(nameFile);
        if (dataFound != null)
            data = dataFound;
    }
    public void NewEvent()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
