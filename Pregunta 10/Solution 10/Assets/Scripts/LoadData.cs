using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class LoadData
{
    public static T Load<T>(/*string path, */string fileName)
    {
        //string fullPath = Application.persistentDataPath + "/" +/* path + "/" +*/ fileName + ".json";
        string fullPath = Application.dataPath + "/" +/* path + "/" +*/ fileName + ".json";
        Debug.Log(fullPath);
        //fullPath =fullPath.Replace("/", @"\" );
        //Debug.Log(fullPath);
        if (File.Exists(fullPath))
        {
            string textJson = File.ReadAllText(fullPath);
            var obj = JsonUtility.FromJson<T>(textJson);
            Debug.Log(obj);
            return obj;
        }
        else
        {
            Debug.Log("not file found on load data");
            return default;
        }
    }
}
