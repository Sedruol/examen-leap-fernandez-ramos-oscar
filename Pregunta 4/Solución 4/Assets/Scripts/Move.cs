using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
    private Button btn;
    private float temp;
    public static bool win;
    // Start is called before the first frame update
    void Start()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() => Tap());
        temp = transform.localRotation.eulerAngles.z;
        win = false;
        //Debug.Log(temp);
    }
    public void Tap()
    {
        temp += 90f;
        //Debug.Log(temp);
        transform.rotation = Quaternion.Euler(0f, 0f, temp);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Result.score += 0.5f;
        //Debug.Log(Result.score);
        if (Result.score >= 11.0f)
        {
            //Debug.Log("You Win");
            win = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Result.score -= 0.5f;
        //Debug.Log(Result.score);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
