using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Result : MonoBehaviour
{
    [SerializeField] private GameObject table;
    [SerializeField] private GameObject menuWin;
    [SerializeField] private Button btnPlay;
    public static float score;

    // Start is called before the first frame update
    void Start()
    {
        score = 0.0f;
        table.SetActive(true);
        menuWin.SetActive(false);
        btnPlay.onClick.AddListener(() => PlayAgain());
    }

    void PlayAgain()
    {
        SceneManager.LoadScene("SampleScene");
    }

    // Update is called once per frame
    void Update()
    {
        if (Move.win)
        {
            table.SetActive(false);
            menuWin.SetActive(true);
        }
    }
}
