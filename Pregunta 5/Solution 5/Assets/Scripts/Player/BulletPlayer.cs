using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.rotation.y == 0)
            transform.position = new Vector2(transform.position.x + 0.1f, transform.position.y);
        else
            transform.position = new Vector2(transform.position.x - 0.1f, transform.position.y);
    }
}
