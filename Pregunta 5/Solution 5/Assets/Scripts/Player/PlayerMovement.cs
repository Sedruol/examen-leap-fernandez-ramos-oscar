using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private GameObject bullet1;
    [SerializeField] private GameObject bullet2;
    private Rigidbody2D rb; 
    private Animator anim;
    private bool run;
    private bool roll;
    private bool jump;
    private bool createbullet;
    private bool attackMelee;
    private bool attackRange;
    private bool attackAir;
    private int comboMelee;
    private int comboRange;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        run = false;
        roll = false;
        jump = false;
        createbullet = false;
        attackMelee = false;
        attackRange = false;
        attackAir = false;
        comboMelee = 1;
        comboRange = 1;
    }
    void VelocityZero()
    {
        rb.velocity = Vector3.zero;
    }
    void Run()
    {
        if (roll == false && jump == false && attackMelee == false && attackRange == false && attackAir == false)
        {
            if (Input.GetKey(KeyCode.A))
            {
                run = true;
                anim.SetBool("run", true);
                rb.velocity = new Vector3(rb.velocity.x - 0.15f, rb.velocity.y, 0f);
                this.gameObject.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                run = true;
                anim.SetBool("run", true);
                rb.velocity = new Vector3(rb.velocity.x + 0.15f, rb.velocity.y, 0f);
                this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            }
            else
            {
                anim.SetBool("run", false);
                VelocityZero();
                run = false;
            }
        }
    }
    void Jump()
    {
        if (roll == false && attackMelee == false && attackRange == false && attackAir == false)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                jump = true;
                anim.SetBool("jump", true);
                anim.SetBool("run", false);
                rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y + 5f, 0f);
            }
        }
    }
    void FinishJump()
    {
        jump = false;
        anim.SetBool("jump", false);
        VelocityZero();
    }
    void Roll()
    {
        if (jump == false && attackAir == false)
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                anim.SetBool("roll", true);
                anim.SetBool("run", false);
                anim.SetBool("attackMelee", false);
                anim.SetBool("attackRange", false);
                anim.SetInteger("comboMelee", 1);
                anim.SetInteger("comboRange", 1);
                comboMelee = 1;
                comboRange = 1;
                attackMelee = false;
                attackRange = false;
                roll = true;
                if (transform.rotation.y == 0)
                    rb.velocity = new Vector3(rb.velocity.x + 10f, rb.velocity.y, 0f);
                else
                    rb.velocity = new Vector3(rb.velocity.x - 10f, rb.velocity.y, 0f);
            }
        }
    }
    void FinishRoll()
    {
        anim.SetBool("roll", false);
        roll = false;
    }
    void AttackMelee()
    {
        if (roll == false && jump == false && run == false && attackRange == false && Input.GetKey(KeyCode.J))
        {
            attackMelee = true;
            anim.SetBool("attackMelee", true);
            switch (comboMelee)
            {
                case 1:
                    anim.SetInteger("comboMelee", 1);
                    break;
                case 2:
                    anim.SetInteger("comboMelee", 2);
                    break;
                case 3:
                    anim.SetInteger("comboMelee", 3);
                    break;
            }
        }
    }
    void AttackRange()
    {
        if (roll == false && jump == false && run == false && attackMelee == false && Input.GetKey(KeyCode.I))
        {
            attackRange = true;
            anim.SetBool("attackRange", true);
            switch (comboRange)
            {
                case 1:
                    anim.SetInteger("comboRange", 1);
                    createbullet = true;
                    break;
                case 2:
                    anim.SetInteger("comboRange", 2);
                    createbullet = true;
                    break;
            }
        }
    }
    void FinishAttackMelee()
    {
        if (Input.GetKey(KeyCode.J) && comboMelee < 3)
            comboMelee++;
        else if (Input.GetKey(KeyCode.J) && comboMelee == 3)
            comboMelee = 1;
        else
        {
            anim.SetBool("attackMelee", false);
            anim.SetInteger("comboMelee", 1);
            comboMelee = 1;
            attackMelee = false;
        }
    }
    void CreateBullet(GameObject bullet)
    {
        GameObject _bullet = Instantiate(bullet) as GameObject;
        if (transform.rotation.y == 0)
            _bullet.transform.position = new Vector2(transform.position.x + 2, transform.position.y);
        else
        {
            _bullet.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
            _bullet.transform.position = new Vector2(transform.position.x - 2, transform.position.y);
        }
    }
    void FinishAttackRange()
    {
        if (Input.GetKey(KeyCode.I) && comboRange < 2)
        {
            CreateBullet(bullet1);
            comboRange++;
        }
        else if (Input.GetKey(KeyCode.I) && comboRange == 2)
        {
            CreateBullet(bullet2);
            comboRange = 1;
        }
        else if (comboRange < 2 && createbullet)
        {
            CreateBullet(bullet1);
            createbullet = false;
            anim.SetBool("attackRange", false);
            anim.SetInteger("comboRange", 1);
            comboRange = 1;
            attackRange = false;
        }
        else if (comboRange == 2 && createbullet)
        {
            CreateBullet(bullet2);
            createbullet = false;
            anim.SetBool("attackRange", false);
            anim.SetInteger("comboRange", 1);
            comboRange = 1;
            attackRange = false;
        }
        else
        {
            anim.SetBool("attackRange", false);
            anim.SetInteger("comboRange", 1);
            comboRange = 1;
            attackRange = false;
        }
    }
    /*void AttackAir()
    {
        if (run ==false && roll == false && attackMelee && attackRange == false)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                jump = true;
                anim.SetBool("jump", true);
                anim.SetBool("run", false);
                rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y + 5f, 0f);
            }
        }
    }
    void FinishAttackAir()
    {

    }*/

    // Update is called once per frame
    void Update()
    {
        Run();
        Jump();
        Roll();
        AttackMelee();
        AttackRange();
    }
}
