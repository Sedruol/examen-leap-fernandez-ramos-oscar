using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Vector3 pos;
    private Vector3 forward;
    private bool up;
    private bool down;
    private bool right;
    private bool left;
    // Start is called before the first frame update
    void Start()
    {
        pos = transform.position;
        up = false;
        down = true;
        right = false;
        left = false;
    }
    void Vision()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            left = true;
            up = false;
            down = false;
            right = false;
            forward = new Vector3(pos.x - 13, pos.y, pos.z);
            Debug.Log("izquierda");
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            left = false;
            up = true;
            down = false;
            right = false;
            forward = new Vector3(pos.x, pos.y + 13, pos.z);
            Debug.Log("arriba");
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            left = false;
            up = false;
            down = true;
            right = false;
            forward = new Vector3(pos.x, pos.y - 13, pos.z);
            Debug.Log("abajo");
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            left = false;
            up = false;
            down = false;
            right = true;
            forward = new Vector3(pos.x + 13, pos.y, pos.z);
            Debug.Log("derecha");
        }
    }
    void SearchEnemy()
    {

    }
    // Update is called once per frame
    void Update()
    {
        Vision();
        /*RaycastHit hit;
        if (left)
        {
            //Gizmos.color = Color.red;
            //Gizmos.DrawRay(pos, forward * 13f);
            if (Physics.Raycast(pos, forward, out hit, 13f))
            {
                if (hit.transform.tag == "Enemy")
                {
                    Debug.Log("I see you");
                }
            }
        }*/
    }
}
