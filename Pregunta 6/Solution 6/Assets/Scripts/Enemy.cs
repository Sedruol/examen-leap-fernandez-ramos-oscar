using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Vector3 position;
    public bool active;
    public bool visible;
    // Start is called before the first frame update
    void Start()
    {
        active = false;
        visible = true;
        position = transform.position;
    }
    /*private void OnBecameInvisible()
    {
        Debug.Log("estoy fuera de pantalla papu");
        visible = false;
    }*/
    private void ValidateVisible()
    {
        if (position.x < -10.45f || position.x > 10.45f)
            visible = false;
        if (position.y < -5.9f || position.y > 5.9f)
            visible = false;
    }
    // Update is called once per frame
    void Update()
    {
        ValidateVisible();
        if (!visible)
            Destroy(this.gameObject);
    }
}
