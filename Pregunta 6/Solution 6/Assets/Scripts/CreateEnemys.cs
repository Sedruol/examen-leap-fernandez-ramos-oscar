using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateEnemys : MonoBehaviour
{
    [SerializeField] private List<Enemy> activeEnemys = new List<Enemy>();
    private void Awake()
    {
        for(int i =0; i < activeEnemys.Count; i++)
        {
            GameObject e = Instantiate(activeEnemys[i].gameObject) as GameObject;
            e.transform.position = new Vector3(Random.Range(-13f, 13f), Random.Range(-7f, 7f), 0f);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
