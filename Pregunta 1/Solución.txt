Al costado del siguiente algoritmo se encuentra comentado el funcionamiento de este mismo:

bool xyz (int n) {					//se crea una función función booleana, por lo cual el resultado será TRUE o FALSE
	float i = math.sqrt(n);				//la variable "i" puede almacenar decimales y se le asignará el valor de la raiz cuadrada de la variable "n"
	int j = math.ceil(i);				//la variable "j" solo almacena números enteros y se le asignará el valor obtenido tras redondear con 									//aproximación al mayor el valor de "i"
	int k = 2;					//se crea la variable entera "k" y se le asigna el valor de 2
	int x = k;					//se crea la variable entera "x" y se le asigna el valor de "k"
	while(x <= j) {					//se entra en un bucle y permanecemos allí siempre y cuando "x" sea menor o igual a "j"
		if(!(n % x))				//se verifica si no existe residuo al dividir "n" y "x", es decir, si el residuo es igual a cero.
			return false;			//de ser el residuo obtenido diferente de cero, el resultado final es FALSE
		else					//si el residuo es diferente a cero
			x++;				//el valor de "x" se incrementa en 1
	}						//si "x" deja de ser menor o igual a "j" se cierra el bucle del while
	return true;					//el resultado final es TRUE
}							//Tras analizar el algoritmo se puede concluir que este sirve para determinar si un número es o no es primo

A continuación se muestra como se desarrollaría el algoritmo en 2 casos:
Ejemplo 1 : Ejecución del algoritmo con "n" = 10.
xyz(10):
	i = 3.1622777
	j = 4
	k = 2
	x = 2
	¿x es menor o igual a j? (2 <= 4, es verdadero; por ende, se abre el bucle)
		10 % 2  brinda un residuo exacto, es decir, el residuo es igual a cero.
	Por ende, el resultado de este algoritmo cuando n = 10, es FALSE
	
Ejemplo 2 : Ejecución del algoritmo con "n" = 7.
xyz(7):
	i = 2.6457513
	j = 3
	k = 2
	x = 2
	¿x es menor o igual a j? (2 <= 3, es verdadero; por ende, se abre el bucle)
		7 % 2  brinda un residuo equivalente a 1, es decir, si existe residuo; debido a esto:
		x = 2+1 = 3
	¿x es menor o igual a j? (3 <= 3, es verdadero; por ende, se abre el bucle)
		7 % 3  brinda un residuo equivalente a 1, es decir, si existe residuo; debido a esto:
		x = 3+1 = 4
	¿x es menor o igual a j? (4 <= 3, es falso; por ende, se cierra el bucle)
	Por ende, el resultado de este algoritmo cuando n = 7, es TRUE
	
	