#include<iostream>
#include<math.h>
using namespace std;

class Rect {
private:
    string name;
    float w;
    float halfW;
    float h;
    float halfH;
    float maxLimit[2];
    float minLimit[2];
    float center[2];

    bool CollisionByCenter(Rect rect) {
        return center[0] == rect.center[0] && center[1] == rect.center[1];
    }
    bool CollisionByLeft(Rect rect) {
        return center[0] < rect.center[0];
    }
    bool CollisionByRight(Rect rect) {
        return center[0] > rect.center[0];
    }
    bool CollisionByUp(Rect rect) {
        return center[1] > rect.center[1];
    }
    bool CollisionByDown(Rect rect) {
        return center[1] < rect.center[1];
    }
public:

    Rect(string _name, float _w, float _h, float _center[2])
    {
        name = _name;
        w = _w;
        h = _h;
        center[0] = _center[0];
        center[1] = _center[1];
        halfW = w / 2;
        halfH = h / 2;
        minLimit[0] = center[0] - halfW;
        maxLimit[0] = center[0] + halfW;
        minLimit[1] = center[1] - halfH;
        maxLimit[1] = center[1] + halfH;
    }

    bool OnCollision(Rect rect){
        if (CollisionByCenter(rect) || 
            ((CollisionByLeft(rect) && maxLimit[0] >= rect.minLimit[0]) || 
                (CollisionByRight(rect) && minLimit[0] <= rect.maxLimit[0])) &&
            ((CollisionByUp(rect) && minLimit[1] <= rect.maxLimit[1]) ||
                (CollisionByDown(rect) && maxLimit[1] >= rect.minLimit[1]))) {
            cout << "Rectangle " << name << " collides with " << rect.name << endl;
            return true;
        }
        else {
            cout << "Rectangle " << name << " does not collide with " << rect.name << endl;
            return false;
        }
    }
};

int main() {
    float c1[2] = { 8.0f, 9.0f };
    float c2[2] = { 2.0f, 5.0f };
    float c3[2] = { 6.0f, 2.0f };
    Rect rect1("1", 10.0f, 4.0f, c1);
    Rect rect2("2", 4.0f, 8.0f, c2);
    Rect rect3("3", 6.0f, 2.0f, c3);

    rect1.OnCollision(rect2);
    rect2.OnCollision(rect3);
    rect3.OnCollision(rect1);
    system("pause");
    return 0;
}