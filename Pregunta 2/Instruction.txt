Este ejercicio se desarrolla en C++ y visual studio 2019.
Para abrir el visual studio seleccione "Eje 2/Eje 2.sln" y una vez cargado, abrá el archivo "Sol2.cpp".
En caso ocurra algún problema por trabajar con otra versión de visual studio, el script se encuentra en "Eje 2/Eje 2/Sol2.cpp".
Dentro del main, se pueden editar los siguientes valores de rect1, rect2 y rect3:
Ejemplo: Rect rect1("1", 10.0f, 4.0f, c1);
	*"1": name
	*10.0f: width
	*4.0f: height
	*c1: punto céntrico del rectángulo