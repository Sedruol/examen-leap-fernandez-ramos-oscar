using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Position : MonoBehaviour
{
    [Header("Prefab que deseas usar como punto")]
    [SerializeField] private GameObject point;
    [Header("Posici�n inicial")]
    [SerializeField] private int x;
    [SerializeField] private int y;
    [Header("Se recomienda usar como m�nimo 2 puntos")]
    [SerializeField] private int num;
    [SerializeField] private int radio;

    private LineRenderer _lineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _lineRenderer.startWidth = 0.1f;
        Pos(x, y, radio, num);
    }
    private void Pos(int x, int y, int r, float n)
    {
        _lineRenderer.positionCount = 181;
        float angle = Mathf.PI / (n-1); //Mathf.PI = 180�, se usa este grado porque es un semicirculo
        for (int i = 0; i < n; i++)
        {
            GameObject e = Instantiate(point) as GameObject;
            e.transform.position = new Vector2(x + r * Mathf.Cos(angle * i), y + r * Mathf.Sin(angle * i));

        }
        for (int j = 0; j <= 180; j++)
        {
            _lineRenderer.SetPosition(j, new Vector3(x + r * Mathf.Cos(j * Mathf.Deg2Rad), y + r * Mathf.Sin(Mathf.Deg2Rad * j), 0));
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Pos(x, y, radio, num);
    }
}
